/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var SCALED_IMAGE_HEIGHT=300;
var IMG_WIDTH=150;

function autoScaleImage() {
	var origHeight = $("#image").height();
	var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
	var origWidth = $("#image").width();
	var scaledWidth = origWidth * scaleFactor;
	$("#image").height(SCALED_IMAGE_HEIGHT);
	$("#image").width(scaledWidth);
	var left = (IMG_WIDTH-scaledWidth)/2;
	$("#image").css("left", left);
}