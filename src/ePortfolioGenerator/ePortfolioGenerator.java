/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator;

import ePortfolioGenerator.Controller.PortfolioFileManager;
import static ePortfolioGenerator.LanguagePropertyType.TITLE_WINDOW;
import static ePortfolioGenerator.StartupConstants.ICON_WINDOW_LOGO;
import static ePortfolioGenerator.StartupConstants.PATH_DATA;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import static ePortfolioGenerator.StartupConstants.PATH_IMAGES;
import static ePortfolioGenerator.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import ePortfolioGenerator.View.PortfolioGeneratorView;
import java.io.File;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Rahul S Agasthya
 */
public class ePortfolioGenerator extends Application {
    
    PortfolioFileManager fileManager = new PortfolioFileManager();
    
    PortfolioGeneratorView ui = new PortfolioGeneratorView(fileManager);
    
    public boolean loadProperties(String languageCode) {
        try {
	    // FIGURE OUT THE PROPER FILE NAME
	    String propertiesFileName = "properties_" + languageCode + ".xml";

	    // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
           //todo
            return false;
        }        
    }
    
    public void start(Stage primaryStage) throws Exception {
        String languageCode = "EN";
        
        // SET THE WINDOW ICON
	String imagePath = PATH_ICONS + ICON_WINDOW_LOGO;
	File file = new File(imagePath);
	
	// GET AND SET THE IMAGE
	URL fileURL = file.toURI().toURL();
	Image windowIcon = new Image(fileURL.toExternalForm());
	primaryStage.getIcons().add(windowIcon);
        
        boolean success = loadProperties(languageCode);
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    //todo
	}
    }
    
    public static void main(String[] args) {
	launch(args);
    }
}
