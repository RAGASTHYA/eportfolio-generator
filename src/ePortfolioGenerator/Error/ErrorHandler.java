package ePortfolioGenerator.Error;

import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_DIALOG_BOX_STYLE;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_TEXT_TITLE_STYLE;
import static ePortfolioGenerator.StartupConstants.STYLE_SHEET_UI;
import ePortfolioGenerator.View.PageEditView;
import ePortfolioGenerator.View.PortfolioGeneratorView;
import ePortfolioGenerator.View.SlideEditView;
import ePortfolioGenerator.View.SlideShowMakerView;
import javafx.geometry.Pos;
import javafx.scene.Scene;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class provides error messages to the user when the occur. Note that
 * error messages should be retrieved from language-dependent XML files and
 * should have custom messages that are different depending on the type of error
 * so as to be informative concerning what went wrong.
 *
 * @author McKilla Gorilla & _____________
 */
public class ErrorHandler {

    // APP UI
    private PortfolioGeneratorView ui;
    private SlideShowMakerView ssUI;
    private PageEditView pageUI;
    private SlideEditView slideUI;

    // KEEP THE APP UI FOR LATER
    public ErrorHandler(PortfolioGeneratorView initUI) {
        ui = initUI;
    }

    public ErrorHandler(SlideShowMakerView initUI) {
        ssUI = initUI;
    }

    public ErrorHandler(PageEditView initUI) {
        pageUI = initUI;
    }

    public ErrorHandler(SlideEditView initUI) {
        slideUI = initUI;
    }

    /**
     * This method provides all error feedback. It gets the feedback text, which
     * changes depending on the type of error, and presents it to the user in a
     * dialog box.
     *
     * @param errorType Identifies the type of error that happened, which allows
     * us to get and display different text for different errors.
     */
    public void processError(String errorDialogTitle, String errorDialogMessage) {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //String errorFeedbackText = props.getProperty(errorType);

        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        // THIS WAS CREATED IN THE RESPECTIVE CLASSES< WHERE IT WAS NECESSARY.
        Stage errorBox = new Stage();
        errorBox.setMinHeight(300);
        errorBox.setMinWidth(700);
        errorBox.setTitle(errorDialogTitle);
        VBox pane = new VBox();
        HBox message = new HBox();
        Text t = new Text();
        t.setText(errorDialogMessage);
        t.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        message.getChildren().add(new ImageView(new Image("file:./images/icons/Error.png")));
        message.setAlignment(Pos.CENTER);
        message.getChildren().add(t);
        Button OK = new Button("OK");
        pane.getChildren().add(message);
        pane.getChildren().add(OK);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        Scene errorScene = new Scene(pane);
        errorScene.getStylesheets().add(STYLE_SHEET_UI);
        errorBox.setScene(errorScene);
        errorBox.show();
        OK.setOnAction(e -> {
            errorBox.close();
        });

    }
}
