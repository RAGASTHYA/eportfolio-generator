/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Controller;

import ePortfolioGenerator.Model.Page;
import static ePortfolioGenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static ePortfolioGenerator.StartupConstants.PATH_VIDEOS;
import ePortfolioGenerator.View.PageEditView;
import java.io.File;
import javafx.stage.FileChooser;

/**
 *
 * @author Rahul S Agasthya
 */
public class VideoSelectionController {
   /**
     * This function provides the response to the user's request to
     * select an image.
     * 
     * @param slideToEdit - Slide for which the user is selecting an image.
     * 
     * @param view The user interface control group where the image
     * will appear after selection.
     */
    public void processSelectVideo(PageEditView view, Page pageModel) {
	FileChooser videoFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	videoFileChooser.setInitialDirectory(new File(PATH_VIDEOS));
	
	// LET'S ONLY SEE VIDEO FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	FileChooser.ExtensionFilter movFilter = new FileChooser.ExtensionFilter("MOV files (*.mov)", "*.MOV");
	FileChooser.ExtensionFilter wmvFilter = new FileChooser.ExtensionFilter("WMV files (*.wmv)", "*.WMV");
	videoFileChooser.getExtensionFilters().addAll(mp4Filter, movFilter, wmvFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = videoFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    pageModel.getSelectedComponent().setMedia_link(file.getPath());
	    view.updateVideo(view.getMediaSelectionView());
	}
    }
}
