package ePortfolioGenerator.Controller;

import ePortfolioGenerator.Controller.SlideShowFileManager;
import ePortfolioGenerator.Error.ErrorHandler;
import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_CANNOT_SAVE;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_JSON_NOT_FOUND;
import static ePortfolioGenerator.LanguagePropertyType.APPLICATION_CRASHED;
import ePortfolioGenerator.Model.PortfolioModel;
import ePortfolioGenerator.Model.SlideShowModel;
import static ePortfolioGenerator.StartupConstants.PATH_PORTFOLIO_SAVE;
import ePortfolioGenerator.View.PageEditView;
import ePortfolioGenerator.View.PortfolioGeneratorView;
import ePortfolioGenerator.View.SlideShowMakerView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.scene.control.Alert.AlertType.ERROR;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

/**
 * This class serves as the controller for all file toolbar operations, driving
 * the loading and saving of slide shows, among other things.
 *
 * @author McKilla Gorilla & _____________
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private PortfolioGeneratorView ui;
    private SlideShowMakerView ssmUI;

    private PropertiesManager props;

    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private PortfolioFileManager portfolioIO;
    private SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file
     * being edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide
     * show data.
     */
    public FileController(PortfolioGeneratorView initUI, PortfolioFileManager initPortfolioIO) {
        // NOTHING YET
        props = PropertiesManager.getPropertiesManager();
        saved = true;
        ui = initUI;
        portfolioIO = initPortfolioIO;
    }

    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        props = PropertiesManager.getPropertiesManager();
        saved = true;
        ssmUI = initUI;
        slideShowIO = initSlideShowIO;
    }

    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewPortfolioRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                PortfolioModel portfolio = ui.getPortfolioModel();
                portfolio.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // MAKE SURE THE TITLE CONTROLS ARE ENABLED	
            }
        } catch (IOException ioe) {
            //ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideshow = ssmUI.getSlideShow();
                slideshow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ssmUI.updateToolbarControls(saved);

                // MAKE SURE THE TITLE CONTROLS ARE ENABLED	
            }
        } catch (IOException ioe) {
            //ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }

    /**
     * This method shows the current slide show in a separate window.
     */
    public void handleViewSlideShowRequest() {
        //SlideShowViewer viewer = new SlideShowViewer(ui);
        //viewer.startSlideShow();
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = true;

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            PortfolioModel portfolio = ui.getPortfolioModel();
            portfolioIO.savePortfolio(portfolio);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen(PageEditView pageUI) {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_PORTFOLIO_SAVE));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                PortfolioModel portfolioToLoad = ui.getPortfolioModel();
                portfolioIO.loadPortfolio(portfolioToLoad, selectedFile.getAbsolutePath(), pageUI);
                ui.reloadPageNavPane();
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                e.printStackTrace();
                //ErrorHandler eH = ui.getErrorHandler();
                //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadPortfolioRequest(PageEditView pageUI) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen(pageUI);
                ui.reloadPageNavPane();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_JSON_NOT_FOUND));
            ui.getWindow().close();
        } catch (Exception ex) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR), props.getProperty(APPLICATION_CRASHED));
            ui.getWindow().close();
        }
    }

    public boolean handleSavePortfolioRequest(PortfolioModel portfolioToSave) {
        try {
            //portfolioToSave = ui.getPortfolioModel();
            portfolioIO.savePortfolio(portfolioToSave);
            return true;
        } catch (FileNotFoundException ex) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_CANNOT_SAVE));
            ui.getWindow().close();
            return false;
        } 
        catch (NullPointerException ex) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_CANNOT_SAVE));
            ui.getWindow().close();
            return false;
        } catch (Exception ex) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR), props.getProperty(APPLICATION_CRASHED));
            ui.getWindow().close();
            return false;
        }
    }

    public boolean handleSaveAsPortfolioRequest(PortfolioModel portfolioToSave, PortfolioGeneratorView ui) {
        try {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(ui.getWindow());

            if (file != null) {
                portfolioIO.savePortfolio(portfolioToSave);
                return true;
            }
            return false;            
        } catch (FileNotFoundException ex) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_CANNOT_SAVE));
            ui.getWindow().close();
            return false;
        }
        catch (NullPointerException ex) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_CANNOT_SAVE));
            ui.getWindow().close();
            return false;
        } catch (Exception ex) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR), props.getProperty(APPLICATION_CRASHED));
            ui.getWindow().close();
            return false;
        }
    }
}
