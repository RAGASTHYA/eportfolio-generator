/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Controller;

import static ePortfolioGenerator.Controller.SlideShowFileManager.JSON_CAPTION;
import static ePortfolioGenerator.Controller.SlideShowFileManager.JSON_EXT;
import static ePortfolioGenerator.Controller.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ePortfolioGenerator.Controller.SlideShowFileManager.JSON_IMAGE_PATH;
import static ePortfolioGenerator.Controller.SlideShowFileManager.JSON_SLIDES;
import static ePortfolioGenerator.Controller.SlideShowFileManager.SLASH;
import ePortfolioGenerator.Model.Component;
import ePortfolioGenerator.Model.Page;
import ePortfolioGenerator.Model.PortfolioModel;
import ePortfolioGenerator.Model.Slide;
import ePortfolioGenerator.Model.SlideShowModel;
import static ePortfolioGenerator.StartupConstants.PATH_PORTFOLIO_SAVE;
import ePortfolioGenerator.View.PageEditView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Rahul S Agasthya
 */
public class PortfolioFileManager {

    public static String JSON_TITLE = "title";
    public static String JSON_SLIDES = "slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    public static String STUDENT_NAME = "student_name";
    public static String PAGE_BANNER = "banner_image";
    public static String PAGES = "pages";
    public static String FOOTER = "footer";

    public static String PAGE_TITLE = "page_title";
    public static String PAGE_STYLE = "page_style";
    public static String PAGE_HAS_BANNER = "has_banner";
    public static String PAGE_COMPOENTS = "components";
    public static String COMPONENT_TYPE = "component_type";

    public static String COMPONENT_HEADING = "Heading";
    public static String COMPONENT_TEXT_BOX = "Text_Box";
    public static String COMPONENT_IMAGE = "Image_Name";
    public static String COMPONENT_VIDEO = "Video_Name";
    public static String COMPONENT_LISTS = "Lists";
    public static String COMPONENT_SLIDE_SHOW = "Slide_Show";

    public static String LIST_ITEM = "List_Item";

    public void savePortfolio(PortfolioModel portfolioToSave) throws FileNotFoundException {
        StringWriter sw = new StringWriter();

        JsonArray pagesJsonArray = makePagesJsonArray(portfolioToSave.getPages());

        JsonObject portfolioJsonObject = Json.createObjectBuilder()
                .add(STUDENT_NAME, portfolioToSave.getStudentName())
                .add(PAGE_BANNER, portfolioToSave.getBannerImageLink())
                .add(PAGES, pagesJsonArray)
                .add(FOOTER, portfolioToSave.getFooter())
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(portfolioJsonObject);
        jsonWriter.close();

        // INIT THE WRITER
        String portfolioTitle = portfolioToSave.getStudentName();
        String jsonFilePath = PATH_PORTFOLIO_SAVE + SLASH + portfolioTitle + JSON_EXT;
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(portfolioJsonObject);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(jsonFilePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    public void savePortfolio(PortfolioModel portfolioToSave, String initPortfolioPath) throws FileNotFoundException {
        StringWriter sw = new StringWriter();

        JsonArray pagesJsonArray = makePagesJsonArray(portfolioToSave.getPages());

        JsonObject portfolioJsonObject = Json.createObjectBuilder()
                .add(STUDENT_NAME, portfolioToSave.getStudentName())
                .add(PAGE_BANNER, portfolioToSave.getBannerImageLink())
                .add(PAGES, pagesJsonArray)
                .add(FOOTER, portfolioToSave.getFooter())
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(portfolioJsonObject);
        jsonWriter.close();

        // INIT THE WRITER
        String jsonFilePath = initPortfolioPath;
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(portfolioJsonObject);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(jsonFilePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    private JsonArray makePagesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
            JsonObject jso = makePageJsonObject(page);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makePageJsonObject(Page page) {
        JsonArray jA = makeComponentsJsonArray(page.getComponents());

        JsonObject jso = Json.createObjectBuilder()
                .add(PAGE_TITLE, page.getPageTitle())
                .add(PAGE_STYLE, page.getPageStyle())
                .add(PAGE_HAS_BANNER, page.isHasBanner())
                .add(PAGE_COMPOENTS, jA)
                .build();
        return jso;
    }

    private JsonObject makeComponentObject(Component component) {
        JsonObject jso = null;
        switch (component.getComponent_type()) {
            case "Heading":
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_HEADING, component.getHeading()).build();
                break;
            case "Text Box":
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_TEXT_BOX, component.getText_box()).build();
                break;
            case "Image":
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_IMAGE, component.getMedia_link())
                        .add(JSON_CAPTION, component.getCaption())
                        .build();
                break;
            case "Video":
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_VIDEO, component.getMedia_link())
                        .add(JSON_CAPTION, component.getCaption())
                        .build();
                break;
            case "List":
                JsonArrayBuilder jsb = Json.createArrayBuilder();
                for (String listItem : component.getLists()) {
                    jso = Json.createObjectBuilder()
                            .add(LIST_ITEM, listItem).build();
                    jsb.add(jso);
                }
                JsonArray jA = jsb.build();
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_LISTS, jA).build();
                break;
            case "SlideShow":
                JsonArray jArray = makeSlideShowJsonObject(component.getSlideshow());
                jso = Json.createObjectBuilder()
                        .add(COMPONENT_TYPE, component.getComponent_type())
                        .add(COMPONENT_SLIDE_SHOW, jArray).build();
                break;
        }
        return jso;
    }

    private JsonObject makeSlideJsonObject(Slide slide) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
                .add(JSON_IMAGE_PATH, slide.getImagePath())
                .add(JSON_CAPTION, slide.getCaption())
                .build();
        return jso;
    }

    private JsonArray makeSlideShowJsonObject(SlideShowModel slideshow) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slideshow.getSlides()) {
            JsonObject json = makeSlideJsonObject(slide);
            jsb.add(json);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    public JsonArray makeComponentsJsonArray(List<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        if (components.size() > 0) {
            for (Component component : components) {
                JsonObject jso = makeComponentObject(component);
                jsb.add(jso);
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    public void loadPortfolio(PortfolioModel portfolioToLoad, String jsonFilePath, PageEditView pageUI) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        Page pageToAdd;
        portfolioToLoad.reset();
        portfolioToLoad.setStudentName(json.getString(STUDENT_NAME));
        portfolioToLoad.setBannerImageLink(json.getString(PAGE_BANNER));
        portfolioToLoad.setFooter(json.getString(FOOTER));

        JsonArray jsonPagesArray = json.getJsonArray(PAGES);
        for (int i = 0; i < jsonPagesArray.size(); i++) {
            ObservableList<Component> components = FXCollections.observableArrayList();
            JsonObject page = jsonPagesArray.getJsonObject(i);
            if (page.getBoolean(PAGE_HAS_BANNER)) {
                pageToAdd = new Page(page.getString(PAGE_TITLE), page.getBoolean(PAGE_HAS_BANNER), page.getString(PAGE_STYLE), pageUI);
                //portfolioToLoad.addPage(pageToAdd.getPageTitle(), true, pageToAdd.getPageStyle(), pageUI);
                pageUI = new PageEditView(pageToAdd, json.getString(STUDENT_NAME), json.getString(FOOTER));
            } else {
                pageToAdd = new Page(page.getString(PAGE_TITLE), page.getString(PAGE_STYLE), pageUI);
                //portfolioToLoad.addPage(pageToAdd.getPageTitle(), pageToAdd.getPageStyle(), pageUI);
                pageUI = new PageEditView(pageToAdd, json.getString(STUDENT_NAME), json.getString(PAGE_BANNER), json.getString(FOOTER));
            }
            pageUI.reloadComponentPane();

            JsonArray jsonComponentsArray = page.getJsonArray(PAGE_COMPOENTS);
            for (int j = 0; j < jsonComponentsArray.size(); j++) {
                JsonObject component = jsonComponentsArray.getJsonObject(j);
                Component newComponent;
                switch (component.getString(COMPONENT_TYPE)) {
                    case "Heading":
                        newComponent = new Component(component.getString(COMPONENT_HEADING));
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                    case "Text Box":
                        newComponent = new Component(component.getString(COMPONENT_TYPE), component.getString(COMPONENT_TEXT_BOX));
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                    case "Image":
                        newComponent = new Component(component.getString(COMPONENT_TYPE), component.getString(COMPONENT_IMAGE), component.getString(JSON_CAPTION));
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                    case "Video":
                        newComponent = new Component(component.getString(COMPONENT_TYPE), component.getString(COMPONENT_VIDEO), component.getString(JSON_CAPTION));
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                    case "List":
                        JsonArray jsonListArray = json.getJsonArray(COMPONENT_LISTS);
                        ObservableList<String> lists = loadLists(jsonListArray);
                        newComponent = new Component(component.getString(COMPONENT_TYPE), lists);
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                    case "SlideShow":
                        ObservableList<Slide> slides = FXCollections.observableArrayList();
                        JsonArray jsonSlidesArray = json.getJsonArray(JSON_SLIDES);
                        for (int k = 0; k < jsonSlidesArray.size(); k++) {
                            JsonObject slideJso = jsonSlidesArray.getJsonObject(i);
                            slides.add(new Slide(slideJso.getString(JSON_IMAGE_FILE_NAME),
                                    slideJso.getString(JSON_IMAGE_PATH),
                                    slideJso.getString(JSON_CAPTION)));
                        }
                        SlideShowModel slideshow = new SlideShowModel(slides);
                        newComponent = new Component(component.getString(COMPONENT_TYPE), slideshow);
                        pageToAdd.add(newComponent);
                        pageUI.reloadComponentPane();
                        break;
                }
            }
            portfolioToLoad.addPage(pageToAdd);
            pageUI.reloadComponentPane();
        }
    }

    private ObservableList<String> loadLists(JsonArray jsonListArray) {
        ObservableList<String> list = FXCollections.observableArrayList();
        for (int k = 0; k < jsonListArray.size(); k++) {
            JsonObject listItem = jsonListArray.getJsonObject(k);
            list.add(listItem.getString(LIST_ITEM));
        }
        return list;
    }
}
