/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Model;

import ePortfolioGenerator.LanguagePropertyType;
import ePortfolioGenerator.View.PageEditView;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

/**
 * This class generates a single page for the ePortfolio Page.
 *
 * @author Rahul S Agasthya
 */
public class Page {

    private String pageTitle;
    private String pageStyle;

    boolean hasBanner;
    ObservableList<Component> components;

    Component selectedComponent;

    boolean isComponentSelected;

    PageEditView pageUI;

    public Page(String initPageTitle, String initPageStyle, PageEditView initPageUI) {
        pageTitle = initPageTitle;
        pageStyle = initPageStyle;
        hasBanner = false;
        components = FXCollections.observableArrayList();
        pageUI = initPageUI;
    }

    public Page(String initPageTitle, boolean initHasBanner, String initPageStyle, PageEditView initPageUI) {
        pageTitle = initPageTitle;
        pageStyle = initPageStyle;
        hasBanner = initHasBanner;
        components = FXCollections.observableArrayList();
        pageUI = initPageUI;
    }

    //ACCESSORS
    public String getPageTitle() {
        return pageTitle;
    }

    public boolean isHasBanner() {
        return hasBanner;
    }

    public ObservableList<Component> getComponents() {
        return components;
    }

    public Component getSelectedComponent() {
        return selectedComponent;
    }

    public String getPageStyle() {
        return pageStyle;
    }

    //MUTATORS
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public void setHasBanner(boolean hasBanner) {
        this.hasBanner = hasBanner;
    }

    public void setComponents(ObservableList<Component> components) {
        this.components = components;
    }

    public void setPageStyle(String pageStyle) {
        this.pageStyle = pageStyle;
    }

    //HELPERS
    public void reset() {
        components = FXCollections.observableArrayList();
        components.clear();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        pageTitle = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        selectedComponent = null;
    }

    public void add(Component newComponent) {
        components.add(newComponent);
        selectedComponent = newComponent;
        isComponentSelected = true;
    }

    public void addHeadingComponent(String initHeading) {
        Component componentToAdd = new Component(initHeading);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void addTextComponent(String text_box) {
        Component componentToAdd = new Component("Paragraph", text_box);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void addImageComponent(String initMedia_link, String initCaption) {
        Component componentToAdd = new Component("Image", initMedia_link, initCaption);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void addVideoComponent(String initMedia_link, String initCaption) {
        Component componentToAdd = new Component("Video", initMedia_link, initCaption);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void addListComponent(ObservableList<String> initLists) {
        Component componentToAdd = new Component("List", initLists);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void addSlideShowComponent(SlideShowModel initSlideshow) {
        Component componentToAdd = new Component("Slide Show", initSlideshow);
        components.add(componentToAdd);
        selectedComponent = componentToAdd;
        isComponentSelected = true;
        pageUI.reloadComponentPane();
    }

    public void removeSelectedComponent() {
        if (isComponentSelected()) {
            components.remove(selectedComponent);
            selectedComponent = null;
            isComponentSelected = false;
            pageUI.reloadComponentPane();
        }
    }

    public void setSelectedComponent(Component selectedComponent) {
        isComponentSelected = true;
        this.selectedComponent = selectedComponent;
    }

    public boolean isComponentSelected() {
        return isComponentSelected;
    }

    public boolean isSelectedComponent(Component testComponent) {
        return testComponent == selectedComponent;
    }

    public String toHTML(String bannerImageLink, String studentName, String footer, String navBar) {
        String s = "<html>\n"
                + "   <head>\n"
                + "       <title>" + pageTitle + "</title>\n"
                + "       <meta charset=\"UTF-8\">\n"
                + "       <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "       <link href=\"./css/" + pageStyle + "Style.css\" rel=\"stylesheet\" type=\"text/css\">\n"
                + "       <link href=\"./css/" + pageStyle + "Layout.css\" rel=\"stylesheet\" type=\"text/css\">\n"
                + "       <link href='https://fonts.googleapis.com/css?family=Bangers|Artifika|Chango|Josefin+Slab|Sigmar+One' rel='stylesheet' type='text/css'>\n" + 
                  "        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n" +
                  "        <script src=\"./js/Image_Js.js\" type=\"text/javascript\"></script>\n" +
                  "        <script src=\"./js/Slide_Show_Js.js\" type=\"text/javascript\"></script>\n" + 
                  "        <script>\n" +
                  "            $(document).ready()\n" +
                  "            {\n" +
                  "                initSlideshow();\n" +
                  "            }\n" +
                  "            window.on\n" +
                  "        </script>\n"
                + "   </head>\n"
                + "   <body>\n";
        if (hasBanner) {
            s = s + "       <div id=\"headers\">\n"
                    + "           <img id=\"banner\" src=\"media_data/" + bannerImageLink + "\"><br>"
                    + "           <a id=\"banner_text\">" + studentName + "</a>"
                    + "       </div>";
        } else {
            s = s + "       <div id=\"headers\">\n"
                    + "           <a id=\"banner_text\">" + studentName + "</a>"
                    + "       </div>";
        }
        s = s + "<div id=\"nav\">\n" + navBar + "</div>\n";
        
        for(int i=0; i<components.size(); i++) {
            s = s + components.get(i).toHTML();        
        }
        s = s + "<div id=\"footer\">" + footer + "</div>\n" +
                "</body>\n" +
                "</html>";
        return s;
    }
}
