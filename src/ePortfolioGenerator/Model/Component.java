/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Rahul S Agasthya
 */
public class Component {

    private String component_type;
    private String heading;
    private String media_link;
    private String caption;
    private String text_box;

    private ObservableList<Hyperlink> text_links;

    private ObservableList<String> lists;

    private SlideShowModel slideshow;

    public Component(String initHeading) {
        component_type = "Heading";
        heading = heading;
    }

    public Component(String initComponent_type, String initMedia_link, String initCaption) {
        component_type = initComponent_type;
        media_link = initMedia_link;
        caption = initCaption;
    }

    public Component(String initComponent_type, String text_box) {
        component_type = initComponent_type;
        component_type = "Text Box";
        text_links = FXCollections.observableArrayList();
    }

    public Component(String initComponent_type, ObservableList<String> initLists) {
        component_type = initComponent_type;
        lists = FXCollections.observableArrayList();
        lists = initLists;
    }

    public Component(String initComponent_type, SlideShowModel initSlideshow) {
        component_type = initComponent_type;
        component_type = "SlideShow";
        slideshow = initSlideshow;
    }

    //GETTERS
    public String getHeading() {
        return heading;
    }

    public String getComponent_type() {
        return component_type;
    }

    public String getMedia_link() {
        return media_link;
    }

    public String getCaption() {
        return caption;
    }

    public String getText_box() {
        return text_box;
    }

    public ObservableList<Hyperlink> getText_links() {
        return text_links;
    }

    public ObservableList<String> getLists() {
        return lists;
    }

    public SlideShowModel getSlideshow() {
        return slideshow;
    }

    //SETTERS
    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setComponent_type(String component_type) {
        this.component_type = component_type;
    }

    public void setMedia_link(String media_link) {
        this.media_link = media_link;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setText_box(String text_box) {
        this.text_box = text_box;
    }

    public void setText_links(ObservableList<Hyperlink> text_links) {
        this.text_links = text_links;
    }

    public void setLists(ObservableList<String> lists) {
        this.lists = lists;
    }

    public void setSlideshow(SlideShowModel slideshow) {
        this.slideshow = slideshow;
    }

    //HELPERS
    public void addHyperlink(String word, String link_address) {
        Hyperlink link = new Hyperlink(word, link_address);
        text_links.add(link);
    }

    public void removeHyperlink(String word, String link_address) {
        Hyperlink link = new Hyperlink(word, link_address);
        text_links.remove(text_links.indexOf(link));
    }

    public String getLink(String word) {
        String link = "";
        for(int i=0; i<text_links.size(); i++) {
            if(text_links.get(i).getText().equals(word))
                link = text_links.get(i).getToLink();
            else
                link = "";
        }
        return link;
    }
    
    public String toHTML() {
        String s = "";
        switch (component_type) {
            case "Heading":
                s = s + "<h2 id=\"page_title\">" + heading + "</h3>\n";
                break;
            case "Text Box":
                s = s + "<div id=\"paragraph\">"
                        + "<p>\n";
                if (!text_links.isEmpty()) {
                    String[] string = text_box.split(" ");
                    for (int i = 0; i < string.length; i++) {
                        String link = getLink(string[i]);
                        if(link != "")
                            s = s + " <a href=\""+link+"\" id=\"external\">" + string[i] + "</a> ";
                        else
                            s = s + string[i] + " ";
                    }
                }
                else
                    s = s + text_box;
                s = s + "\n</p>"
                        + "</div>\n";
                break;
            case "Image":
                s = s + "<div id=\"image\">"
                        + "   <img id=\"image\" src=\"media_data/" + media_link.substring(media_link.lastIndexOf("\\")) + "\" onload=\"autoScaleImage()\">\n"
                        + "   <div id=\"image_content\">\n"
                        + "       <p>" + caption + "</p>\n"
                        + "   </div>"
                        + "</div>\n";
                break;
            case "Video":
                s = s + "<div id=\"video\">"
                        + "   <video id=\"video\" width=\"400\" controls>\n"
                        + "       <source src=\"" + media_link + "\" type=\"video/mp4\">\n"
                        + "   </video>"
                        + "   <div id=\"video_content\">\n"
                        + "       <p>" + caption + "</p>\n"
                        + "   </div>\n"
                        + "</div>\n";
                break;
            case "SlideShow":
                s = s + slideshow.toHTML();
                break;
            case "List":
                s = "<div id=\"list\"\n"
                        + "   <ul id=\"list\">\n";
                for (String item : lists) {
                    s = s + "       <li>" + item + "</li>\n";
                }
                s = s + "   </ul>\n </div>\n";
                break;
        }
        return s;
    }
}
