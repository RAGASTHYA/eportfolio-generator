/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Model;

/**
 *
 * @author Rahul
 */
public class Hyperlink {
    private String text;
    private String toLink;

    public Hyperlink(String text, String toLink) {
        this.text = text;
        this.toLink = toLink;
    }

    //ACCESSORS
    public String getText() {
        return text;
    }

    public String getToLink() {
        return toLink;
    }
    
    //MUTATORS

    public void setText(String text) {
        this.text = text;
    }

    public void setToLink(String toLink) {
        this.toLink = toLink;
    }

    public String toHTML(){
        return "<a href=\""+toLink+"\">"+text+"</a>";
    }
}
