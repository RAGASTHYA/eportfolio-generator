/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.Model;

import ePortfolioGenerator.Error.ErrorHandler;
import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_IMAGE_NOT_FOUND;
import static ePortfolioGenerator.LanguagePropertyType.ERROR;
import static ePortfolioGenerator.StartupConstants.DEFAULT_BANNER_IMAGE;
import static ePortfolioGenerator.StartupConstants.DEFAULT_FOOTER;
import static ePortfolioGenerator.StartupConstants.DEFAULT_STUDENT_NAME;
import ePortfolioGenerator.View.PageEditView;
import ePortfolioGenerator.View.PortfolioGeneratorView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import static javafx.collections.FXCollections.observableArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Rahul S Agasthya
 */
public class PortfolioModel {

    private String studentName;
    private String bannerImageLink;
    private String footer;

    private ObservableList<Page> pages;

    private Page page;
    private Page openPage;

    PortfolioGeneratorView ui;

    public PortfolioModel(PortfolioGeneratorView initUi) {
        ui = initUi;
        pages = FXCollections.observableArrayList();
        studentName = DEFAULT_STUDENT_NAME;
        bannerImageLink = DEFAULT_BANNER_IMAGE;
        footer = DEFAULT_FOOTER;
    }

    //ACCESSORS
    public String getStudentName() {
        return studentName;
    }

    public String getBannerImageLink() {
        return bannerImageLink;
    }

    public ObservableList<Page> getPages() {
        return pages;
    }

    public Page getOpenPage() {
        return openPage;
    }

    public String getFooter() {
        return footer;
    }

    //MUTATORS
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setBannerImageLink(String bannerImageLink) {
        this.bannerImageLink = bannerImageLink;
    }

    public void setPages(ObservableList<Page> pages) {
        this.pages = pages;
    }

    public void setOpenPage(Page openPage) {
        this.openPage = openPage;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    //HELPERS
    public boolean isPageOpen() {
        return openPage != null;
    }

    public boolean isOpenPage(Page testPage) {
        return testPage == openPage;
    }

    public void addPage(Page initPage) {
        pages.add(initPage);
        openPage = page;
        ui.reloadPageNavPane();
    }
    
    public void addPage(String initPageTitle, String initPageStyle, PageEditView initPageUI) {
        page = new Page(initPageTitle, initPageStyle, initPageUI);
        pages.add(page);
        openPage = page;
        ui.reloadPageNavPane();
    }

    public void addPage(String initPageTitle, boolean initHasBanner, String initPageStyle, PageEditView initPageUI) {
        page = new Page(initPageTitle, initHasBanner, initPageStyle, initPageUI);
        pages.add(page);
        openPage = page;
        bannerImageLink = DEFAULT_BANNER_IMAGE;
        ui.reloadPageNavPane();
    }

    public void reset() {
        pages.clear();
        pages = FXCollections.observableArrayList();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        studentName = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        openPage = null;
        ui.reloadPageNavPane();
    }

    public void removeSelectedPage() {
        if (isPageOpen()) {
            pages.remove(openPage);
            openPage = null;
            ui.reloadPageNavPane();
        }
    }

    public String generateNavPane(boolean horizontal) {
        String s = "";
        if (horizontal) {
            for (int i = 0; i < pages.size(); i++) {
                s = s + "<a href=\"" + pages.get(i).getPageTitle() + ".html\" id=\"link\">" + pages.get(i).getPageTitle() + "</a>\n";
            }
        } else {
            for (int i = 0; i < pages.size(); i++) {
                s = s + "<a href=\"" + pages.get(i).getPageTitle() + ".html\" id=\"link\">" + pages.get(i).getPageTitle() + "</a><br>\n";
            }
        }
        return s;
    }

    public File generateWebPages(int pageIndex) {
        try {
            File files = new File("sites/" + studentName + "'s Portfolio");
            if (files.exists()) {
                files.delete();
            }
            files.mkdir();

            File MediaFileDir = new File("sites/" + studentName + "'s Portfolio/media_data");
            MediaFileDir.mkdir();
            for (int i = 0; i < pages.size(); i++) {
                for (int j = 0; j < pages.get(i).getComponents().size(); j++) {
                    if (pages.get(i).getComponents().get(j).getComponent_type() == "Image") {
                        String[] media = pages.get(i).getComponents().get(j).getMedia_link().split("\\\\");
                        try {
                            String destination = "sites/" + studentName + "'s Portfolio/media_data/" + media[media.length-1];
                            String source = pages.get(i).getComponents().get(j).getMedia_link();
                            Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
                        } catch (NoSuchFileException nsfe) {
                            ErrorHandler eH = ui.getErrorHandler();
                            //@todo provide error message
                            PropertiesManager props = PropertiesManager.getPropertiesManager();
                            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                        } catch (IOException ex) {
                            ErrorHandler eH = ui.getErrorHandler();
                            //@todo provide error message
                            PropertiesManager props = PropertiesManager.getPropertiesManager();
                            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                        }
                    }
                    if (pages.get(i).getComponents().get(j).getComponent_type() == "Video") {
                        String[] media = pages.get(i).getComponents().get(j).getMedia_link().split("/");
                        try {
                            String destination = "sites/" + studentName + "'s Portfolio/media_data/" + media[media.length-1];
                            String source = pages.get(i).getComponents().get(j).getMedia_link();
                            Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
                        } catch (NoSuchFileException nsfe) {
                            ErrorHandler eH = ui.getErrorHandler();
                            //@todo provide error message
                            PropertiesManager props = PropertiesManager.getPropertiesManager();
                            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                        } catch (IOException ex) {
                            ErrorHandler eH = ui.getErrorHandler();
                            //@todo provide error message
                            PropertiesManager props = PropertiesManager.getPropertiesManager();
                            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                        }
                    }
                }
            }

            File CSSFileDir = new File("sites/" + studentName + "'s Portfolio/css");
            CSSFileDir.mkdir();
            for (int i = 1; i <= 5; i++) {
                try {
                    String destination = "sites/" + studentName + "'s Portfolio/css/Style" + i + "Style.css";
                    String source = "./Data/WebpageCSS/Style" + i + "Style.css";
                    Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    ErrorHandler eH = ui.getErrorHandler();
                    //@todo provide error message
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    //eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                    System.out.println("Error in transferring the css files.");
                }

                try {
                    String destination = "sites/" + studentName + "'s Portfolio/css/Style" + i + "Layout.css";
                    String source = "./Data/WebpageCSS/Style" + i + "Style.css";
                    Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    ErrorHandler eH = ui.getErrorHandler();
                    //@todo provide error message
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    //eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
                    System.out.println("Error in transferring the css files.");
                }
            }

            File JSFileDir = new File("sites/" + studentName + "'s Portfolio/js");
            JSFileDir.mkdir();
            try {
                String destination = "sites/" + studentName + "'s Portfolio/js/Slide_Show_Js.js";
                String source = "./Data/JavaScriptFiles/Slide_Show_Js.js";
                Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                ErrorHandler eH = ui.getErrorHandler();
                //@todo provide error message
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
            }
            
            
            try {
                String destination = "sites/" + studentName + "'s Portfolio/js/Image_Js.js";
                String source = "./Data/JavaScriptFiles/Image_Js.js";
                Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                ErrorHandler eH = ui.getErrorHandler();
                //@todo provide error message
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
            }

            String[] media = bannerImageLink.split("/");
            try {
                String destination = "sites/" + studentName + "'s Portfolio/media_data/" + media[media.length - 1];
                String source = bannerImageLink;
                Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                ErrorHandler eH = ui.getErrorHandler();
                //@todo provide error message
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
            }

            for (Page page : pages) {
                File HTMLFile = new File("sites/" + studentName + "'s Portfolio/" + page.getPageTitle() + ".html");
                HTMLFile.createNewFile();
                FileWriter fWriter = new FileWriter(HTMLFile);
                BufferedWriter writer = new BufferedWriter(fWriter);
                String s = page.toHTML(media[media.length - 1], studentName, footer, generateNavPane(!(page.getPageStyle() == "Style3") || (page.getPageStyle() == "Style4")));
                writer.write(s);
                System.out.println(s);
                writer.close();
                fWriter.close();
            }

            File HTMLFile = new File("sites/" + studentName + "'s Portfolio/" + pages.get(pageIndex).getPageTitle() + ".html");
            
            return HTMLFile;
        } catch (IOException ex) {
            ErrorHandler eH = ui.getErrorHandler();
            //@todo provide error message
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
            return null;
        }
    }
}
