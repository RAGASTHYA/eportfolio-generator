package ePortfolioGenerator.View;

import ePortfolioGenerator.Controller.FileController;
import ePortfolioGenerator.Controller.SlideShowFileManager;
import ePortfolioGenerator.Error.ErrorHandler;
import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.LanguagePropertyType.LABEL_SLIDESHOW_TITLE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import ePortfolioGenerator.Model.Slide;
import ePortfolioGenerator.Model.SlideShowModel;
import ePortfolioGenerator.Controller.FileController;
import ePortfolioGenerator.Controller.SlideShowEditController;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EXIT_SLIDESHOW;
import ePortfolioGenerator.Model.Component;
import ePortfolioGenerator.Model.Page;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_PAGE_TOOLBAR_HBOX;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_SLIDE;
import static ePortfolioGenerator.StartupConstants.ICON_EXIT;
import static ePortfolioGenerator.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_MOVE_DOWN;
import static ePortfolioGenerator.StartupConstants.ICON_MOVE_UP;
import static ePortfolioGenerator.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_REMOVE_SLIDE;
import static ePortfolioGenerator.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import static ePortfolioGenerator.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class provides the User Interface for this application, providing
 * controls and the entry points for creating, loading, saving, editing, and
 * viewing slide shows.
 *
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button exitButton;

    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;

    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;

    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;

    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    Page pageModel;

    PageEditView pageUI;

    boolean loaded;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager, Page initPageModel, PageEditView pageUI) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;

        // MAKE THE DATA MANAGING MODEL
        slideShow = new SlideShowModel(this);

        // WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
        errorHandler = new ErrorHandler(this);

        pageModel = initPageModel;

        this.pageUI = pageUI;
    }

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager, SlideShowModel initSlideShow, Page initPageModel, PageEditView pageUI) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;

        // MAKE THE DATA MANAGING MODEL
        slideShow = initSlideShow;

        // WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
        errorHandler = new ErrorHandler(this);

        pageModel = initPageModel;

        this.pageUI = pageUI;
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
        return slideShow;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle, boolean load) {

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        loaded = load;

        if (loaded) {
            slidesEditorPane = new VBox();
            slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
            initTitleControls();
        }

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
        updateToolbarControls(true);

            // NOW PUT THESE TWO IN THE WORKSPACE
            reloadSlideShowPane();
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
        // FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
        workspace = new HBox();

        // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        slideEditToolbar = new VBox();
        slideEditToolbar.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_HBOX);
        addSlideButton = this.initChildButton(slideEditToolbar, ICON_ADD_SLIDE, TOOLTIP_ADD_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveSlideUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveSlideDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);

        // AND THIS WILL GO IN THE CENTER
        slidesEditorPane = new VBox();
        slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        initTitleControls();

        // NOW PUT THESE TWO IN THE WORKSPACE
        workspace.getChildren().add(slideEditToolbar);
        workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(this, fileManager);
        newSlideShowButton.setOnAction(e -> {
            fileController.handleNewSlideShowRequest();
        });
        exitButton.setOnAction(e -> {
            if (!loaded) {
                Component newComponent = new Component("SlideShow", slideShow);
                pageModel.add(newComponent);
                System.out.println(pageModel.getSelectedComponent());
                primaryStage.close();
                pageUI.reloadComponentPane();
            }
            else {
                Component newComponent = new Component("SlideShow", slideShow);
                pageModel.setSelectedComponent(newComponent);
                primaryStage.close();
                pageUI.reloadComponentPane();
            }
        });

        // THEN THE SLIDE SHOW EDIT CONTROLS
        editController = new SlideShowEditController(this);
        addSlideButton.setOnAction(e -> {
            editController.processAddSlideRequest();
        });
        removeSlideButton.setOnAction(e -> {
            editController.processRemoveSlideRequest();
        });
        moveSlideUpButton.setOnAction(e -> {
            editController.processMoveSlideUpRequest();
        });
        moveSlideDownButton.setOnAction(e -> {
            editController.processMoveSlideDownRequest();
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_HBOX);
        newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW, TOOLTIP_NEW_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT_SLIDESHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        ssmPane = new BorderPane();
        ssmPane.setTop(fileToolbarPane);
        primaryScene = new Scene(ssmPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    /**
     * Updates the enabled/disabled status of all toolbar buttons.
     *
     * @param saved
     */
    public void updateToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        ssmPane.setCenter(workspace);

        updateSlideshowEditToolbarControls();
    }

    public void updateSlideshowEditToolbarControls() {
        // AND THE SLIDESHOW EDIT TOOLBAR
        addSlideButton.setDisable(false);
        boolean slideSelected = slideShow.isSlideSelected();
        removeSlideButton.setDisable(!slideSelected);
        moveSlideUpButton.setDisable(!slideSelected);
        moveSlideDownButton.setDisable(!slideSelected);
    }

    /**
     * Uses the slide show data to reload all the components for slide editing.
     *
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane() {
        slidesEditorPane.getChildren().clear();
        for (Slide slide : slideShow.getSlides()) {
            SlideEditView slideEditor = new SlideEditView(slide);
            if (slideShow.isSelectedSlide(slide)) {
                slideEditor.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
            } else {
                slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
            }
            slidesEditorPane.getChildren().add(slideEditor);
            slideEditor.setOnMousePressed(e -> {
                slideShow.setSelectedSlide(slide);
                this.reloadSlideShowPane();
            });
        }
        updateSlideshowEditToolbarControls();
    }

    private void initTitleControls() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelPrompt = props.getProperty(LABEL_SLIDESHOW_TITLE);
        titlePane = new FlowPane();
        titleLabel = new Label(labelPrompt);
        titleTextField = new TextField();

        titlePane.getChildren().add(titleLabel);
        titlePane.getChildren().add(titleTextField);

        String titlePrompt = props.getProperty(LanguagePropertyType.LABEL_SLIDESHOW_TITLE);
        titleTextField.setText(titlePrompt);

        titleTextField.textProperty().addListener(e -> {
            slideShow.setTitle(titleTextField.getText());
        });
    }
}
