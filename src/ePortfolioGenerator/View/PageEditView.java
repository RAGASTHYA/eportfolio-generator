/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.View;

import ePortfolioGenerator.Controller.ImageSelectionController;
import ePortfolioGenerator.Controller.SlideShowFileManager;
import ePortfolioGenerator.Controller.VideoSelectionController;
import ePortfolioGenerator.Error.ErrorHandler;
import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_IMAGE_NOT_FOUND;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_JSON_NOT_FOUND;
import static ePortfolioGenerator.LanguagePropertyType.ALERT_VIDEO_NOT_FOUND;
import static ePortfolioGenerator.LanguagePropertyType.DEFAULT_HEADING;
import static ePortfolioGenerator.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;
import static ePortfolioGenerator.LanguagePropertyType.DEFAULT_TEXT;
import static ePortfolioGenerator.LanguagePropertyType.TITLE_WINDOW;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_HEADING;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_HYPERLINK;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_IMAGE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_LIST;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_LIST_ITEM;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_SLIDE_SHOW;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_TEXT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_VIDEO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_BROWSE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EDIT_HYPERLINK;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_REMOVE_COMPONENT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_REMOVE_LIST_ITEM;
import ePortfolioGenerator.Model.Component;
import ePortfolioGenerator.Model.Hyperlink;
import ePortfolioGenerator.Model.Page;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_DIALOG_BOX_STYLE;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_PAGE_TOOLBAR_HBOX;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_TEXT_TITLE_STYLE;
import static ePortfolioGenerator.StartupConstants.DEFAULT_BANNER_WIDTH;
import static ePortfolioGenerator.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ePortfolioGenerator.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ePortfolioGenerator.StartupConstants.DEFAULT_VIDEO;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_HEADING;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_HYPERLINK;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_IMAGE;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_LIST;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_SLIDE;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_TEXT;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_VIDEO;
import static ePortfolioGenerator.StartupConstants.ICON_EDIT_HYPERLINK;
import static ePortfolioGenerator.StartupConstants.ICON_REMOVE_COMPONENT;
import static ePortfolioGenerator.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import static ePortfolioGenerator.StartupConstants.PATH_IMAGES;
import static ePortfolioGenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static ePortfolioGenerator.StartupConstants.PATH_VIDEOS;
import static ePortfolioGenerator.StartupConstants.STYLE_SHEET_UI;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import static javafx.scene.control.Alert.AlertType.ERROR;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaView;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Rahul
 */
public class PageEditView extends VBox {

    VBox headers;

    Label pageTitle;
    Label studentName;
    Label footer;
    ImageView bannerImageView;

    HBox componentControls;

    Button addImage;
    Button addVideo;
    Button addSlideShow;
    Button addText;
    Button addList;
    Button removeComponent;
    Button addHeading;

    VBox components;
    ScrollPane componentPane;

    Page pageModel;

    ArrayList<String> componentArray;
    HBox componentView;

    ImageView imageSelectionView;
    MediaView mediaSelectionView;
    Component newComponent;

    ObservableList<String> listComponent;
    String listItem;

    PropertiesManager props = PropertiesManager.getPropertiesManager();

    public ImageView getImageSelectionView() {
        return imageSelectionView;
    }

    public MediaView getMediaSelectionView() {
        return mediaSelectionView;
    }

    public PageEditView(Page initPage, String initStudentName, String initFooter) {
        headers = new VBox();
        this.pageTitle = new Label(initPage.getPageTitle());
        this.pageTitle.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        this.studentName = new Label(initStudentName);
        this.studentName.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        pageModel = initPage;
        headers.getChildren().addAll(pageTitle, studentName);
        componentControls = new HBox();
        pageControl();
        components = new VBox();
        footer = new Label(initFooter);
        this.footer.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        reloadComponentPane();
        getChildren().addAll(headers, componentControls, componentPane, footer);
        pageControlEventHandlers();
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
    }

    public PageEditView(Page initPage, String initStudentName, String initBannerImage, String initFooter) {
        headers = new VBox();
        this.pageTitle = new Label(initPage.getPageTitle());
        this.pageTitle.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        this.studentName = new Label(initStudentName);
        this.studentName.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        pageModel = initPage;

        bannerImageView = new ImageView();
        updateBannerImage(initBannerImage);
        headers.getChildren().addAll(bannerImageView, pageTitle, studentName);
        pageControl();
        components = new VBox();
        footer = new Label(initFooter);
        this.footer.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        reloadComponentPane();
        getChildren().addAll(headers, componentControls, componentPane, footer);
        pageControlEventHandlers();
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
    }

    public PageEditView(String initPageTitle, String initStudentName, String initFooter) {
        headers = new VBox();
        this.pageTitle = new Label(initPageTitle);
        this.pageTitle.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        this.studentName = new Label(initStudentName);
        this.studentName.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        pageModel = new Page(initPageTitle, "", this);
        headers.getChildren().addAll(pageTitle, studentName);

        componentControls = new HBox();
        pageControl();
        components = new VBox();
        footer = new Label(initFooter);
        this.footer.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        reloadComponentPane();
        getChildren().addAll(headers, componentControls, componentPane, footer);
        pageControlEventHandlers();
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
    }

    public PageEditView(String initPageTitle, String initStudentName, String initBanner, String initFooter) {
        headers = new VBox();
        this.pageTitle = new Label(initPageTitle);
        this.pageTitle.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        this.studentName = new Label(initStudentName);
        this.studentName.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        pageModel = new Page(initPageTitle, true, "", this);

        bannerImageView = new ImageView();
        updateBannerImage(initBanner);
        headers.getChildren().addAll(bannerImageView, pageTitle, studentName);
        componentControls = new HBox();
        pageControl();
        components = new VBox();
        footer = new Label(initFooter);
        reloadComponentPane();
        this.footer.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        getChildren().addAll(headers, componentControls, componentPane, footer);
        pageControlEventHandlers();
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
    }

    public void updateBannerImage(String initImage) {
        String imagePath = initImage;
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image bannerImage = new Image(fileURL.toExternalForm());
            bannerImageView.setImage(bannerImage);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_BANNER_WIDTH;
            double perc = scaledWidth / bannerImage.getWidth();
            double scaledHeight = bannerImage.getHeight() * perc;
            bannerImageView.setFitWidth(scaledWidth);
            bannerImageView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            //todo
        }
    }

    public void pageControl() {
        componentControls = new HBox();

        componentControls.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_HBOX);
        addHeading = this.initChildButton(componentControls, ICON_ADD_HEADING, TOOLTIP_ADD_HEADING, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        addImage = this.initChildButton(componentControls, ICON_ADD_IMAGE, TOOLTIP_ADD_IMAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        addText = this.initChildButton(componentControls, ICON_ADD_TEXT, TOOLTIP_ADD_TEXT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        addVideo = this.initChildButton(componentControls, ICON_ADD_VIDEO, TOOLTIP_ADD_VIDEO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        addSlideShow = this.initChildButton(componentControls, ICON_ADD_SLIDE_SHOW, TOOLTIP_ADD_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        addList = this.initChildButton(componentControls, ICON_ADD_LIST, TOOLTIP_ADD_LIST, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    public void reloadComponentPane() {
        components.getChildren().clear();
        System.out.println(pageModel.getComponents().size());
        for (Component component : pageModel.getComponents()) {
            componentView = new HBox();
            String componentType = component.getComponent_type();
            Label componentName = new Label(componentType);
            componentView.getChildren().add(componentName);
            componentView.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            removeComponent = this.initChildButton(componentView, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_COMPONENT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
            components.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            if (pageModel.isSelectedComponent(component)) {
                componentView.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
                removeComponent.setDisable(false);
            } else {
                componentView.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
                removeComponent.setDisable(true);
            }
            components.getChildren().add(componentView);
            components.minWidth(DEFAULT_BANNER_WIDTH);

            componentName.setOnMouseClicked(e -> {
                pageModel.setSelectedComponent(component);
                if (component.getComponent_type() == "Heading") {
                    updateHeading();
                }
                if (component.getComponent_type() == "Text Box") {
                    updateText();
                }
                if (component.getComponent_type() == "Image") {
                    ImageInput();
                }
                if (component.getComponent_type() == "Video") {
                    VideoInput();
                }
                if (component.getComponent_type() == "List") {
                    inputList(false);
                }
                if (component.getComponent_type() == "SlideShow") {
                    Stage newStage = new Stage();
                    SlideShowFileManager fileManager = new SlideShowFileManager();
                    SlideShowMakerView ui = new SlideShowMakerView(fileManager, component.getSlideshow(), pageModel, this);
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    String appTitle = props.getProperty(TITLE_WINDOW);
                    ui.startUI(newStage, appTitle, true);
                }
                reloadComponentPane();
            });
            removeComponent.setOnAction(f -> {
                pageModel.removeSelectedComponent();
                reloadComponentPane();
            });
        }
        componentPane = new ScrollPane(components);
    }

    public void pageControlEventHandlers() {
        addHeading.setOnAction(e -> {
            newComponent = new Component(props.getProperty(DEFAULT_HEADING));
            pageModel.add(newComponent);
            addHeading();
            reloadComponentPane();
        });
        addImage.setOnAction(e -> {
            newComponent = new Component("Image", PATH_SLIDE_SHOW_IMAGES + DEFAULT_SLIDE_IMAGE, props.getProperty(DEFAULT_IMAGE_CAPTION));
            pageModel.add(newComponent);
            ImageInput();
            reloadComponentPane();
        });
        addVideo.setOnAction(e -> {
            newComponent = new Component("Video", PATH_VIDEOS + DEFAULT_VIDEO, props.getProperty(DEFAULT_IMAGE_CAPTION));
            pageModel.add(newComponent);
            VideoInput();
            reloadComponentPane();
        });
        addText.setOnAction(e -> {
            newComponent = new Component("Text Box", props.getProperty(DEFAULT_TEXT));
            pageModel.add(newComponent);
            addText();
            reloadComponentPane();
        });
        addList.setOnAction(e -> {
            inputList(true);
            reloadComponentPane();
        });
        addSlideShow.setOnAction(e -> {
            addSlideShow();
            reloadComponentPane();
        });
    }

    public void addSlideShow() {
        Stage newStage = new Stage();
        SlideShowFileManager fileManager = new SlideShowFileManager();
        SlideShowMakerView ui = new SlideShowMakerView(fileManager, pageModel, this);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String appTitle = props.getProperty(TITLE_WINDOW);
        ui.startUI(newStage, appTitle, false);
    }

    public void dialogBox(String message) {
        Stage newStage = new Stage();
        BorderPane pane = new BorderPane();
        Label label = new Label(message);
        pane.setCenter(label);
        Button okay = new Button("OK");
        pane.setBottom(okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
        });
    }

    public void ImageInput() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        HBox imageSelectionWindow = new HBox();
        VBox captionVBox;
        Label captionLabel, imgHeight, imgWidth;
        TextField captionTextField, imgHeightField, imgWidthField;
        ImageSelectionController imageController;

        imageSelectionView = new ImageView();
        updateImage(imageSelectionView);

        Button okay = new Button("OK");

        captionVBox = new VBox();
        //Component image = new Component("Image", DEFAULT_SLIDE_IMAGE, props.getProperty(DEFAULT_IMAGE_CAPTION));
        captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
        captionTextField = new TextField();
        if (pageModel.getSelectedComponent().getCaption() != null) {
            captionTextField.setText(pageModel.getSelectedComponent().getCaption());
        }
        imgHeight = new Label("Image Height");
        imgHeightField = new TextField();
        imgHeightField.setText(Double.toString(imageSelectionView.getFitWidth()));
        imgWidth = new Label("Image Width");
        imgWidthField = new TextField();
        imgWidthField.setText(Double.toString(imageSelectionView.getFitHeight()));

        Label floatCB = new Label("Choose ONE Image Float");
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList("Right",
                "Left",
                "Neither"));
        captionVBox.getChildren().add(captionLabel);
        captionVBox.getChildren().add(captionTextField);
        captionVBox.getChildren().addAll(imgHeight, imgHeightField, imgWidth, imgWidthField, floatCB, cb);
        captionVBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        imageSelectionWindow.getChildren().add(imageSelectionView);
        imageSelectionWindow.getChildren().add(captionVBox);

        // SETUP THE EVENT HANDLERS
        imageController = new ImageSelectionController();
        imageSelectionView.setOnMousePressed(e -> {
            imageController.processSelectImage(this, pageModel);
        });
        pane.getChildren().addAll(imageSelectionWindow, okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setCaption(captionTextField.getText());
        });
    }

    TextField captionTextField;

    public void VideoInput() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        VBox captionVBox;
        Label url, captionLabel;
        TextField urlField;

        Button okay = new Button("OK");
        HBox videoLinkBar = new HBox();
        videoLinkBar.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);

        captionVBox = new VBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        url = new Label("Enter the Video URL");
        urlField = new TextField();
        urlField.setDisable(true);
        videoLinkBar.getChildren().add(urlField);
        Button browse = this.initChildButton(videoLinkBar, ICON_VIEW_SLIDE_SHOW, TOOLTIP_BROWSE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
        captionTextField = new TextField();
        captionTextField.setText(pageModel.getSelectedComponent().getCaption());
        urlField.setText(pageModel.getSelectedComponent().getMedia_link());
        captionVBox.getChildren().add(url);
        captionVBox.getChildren().add(videoLinkBar);
        captionVBox.getChildren().add(captionLabel);
        captionVBox.getChildren().add(captionTextField);
        captionVBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        pane.getChildren().addAll(captionVBox, okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        VideoSelectionController videoController = new VideoSelectionController();
        browse.setOnMousePressed(e -> {
            videoController.processSelectVideo(this, pageModel);
        });
        okay.setOnAction(e -> {
            pageModel.getSelectedComponent().setCaption(captionTextField.getText());
            newStage.close();
        });
    }

    /**
     * This function gets the image for the slide and uses it to update the
     * image displayed.
     */
    public void updateImage(ImageView imageSelectionView) {
        String imagePath = pageModel.getSelectedComponent().getMedia_link();
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image image = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(image);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / image.getWidth();
            double scaledHeight = image.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            ErrorHandler eH = new ErrorHandler(this);
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_IMAGE_NOT_FOUND));
        }
    }

    public void addHeading() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        Label label = new Label("ENTER HEADING");
        TextField title = new TextField();
        title.setText(pageModel.getSelectedComponent().getHeading());
        Button okay = new Button("OK");
        Label message = new Label("Action Event to update the Heading.");
        pane.getChildren().addAll(label, title, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setHeading(title.getText());
            reloadComponentPane();
        });
    }

    public void updateHeading() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        Label label = new Label("ENTER HEADING");
        TextField title = new TextField();
        if (pageModel.getComponents().size() > 0 && pageModel.getSelectedComponent().getHeading() != null) {
            title.setText(pageModel.getSelectedComponent().getHeading());
        }
        Button okay = new Button("OK");
        Label message = new Label("Action Event to update the Heading.");
        pane.getChildren().addAll(label, title, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setHeading(title.getText());
            reloadComponentPane();
        });
    }

    public void addText() {
        Stage newStage = new Stage();
        VBox pane = new VBox();

        Label label = new Label("ENTER THE TEXT");
        TextArea text = new TextArea();
        text.setText(pageModel.getSelectedComponent().getText_box());
        Button okay = new Button("OK");
        Label message = new Label("Action Event to update the Heading.");
        pane.getChildren().addAll(label, text, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setText_box(text.getText());
            reloadComponentPane();
        });
    }

    public void updateText() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        HBox textEdit = new HBox();
        textEdit.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_HBOX);

        Button addHyperlink;
        Button editHyperlink;

        addHyperlink = this.initChildButton(textEdit, ICON_ADD_HYPERLINK, TOOLTIP_ADD_HYPERLINK, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editHyperlink = this.initChildButton(textEdit, ICON_EDIT_HYPERLINK, TOOLTIP_EDIT_HYPERLINK, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);

        Text font1 = new Text("Lato");
        font1.setStyle("-fx-font-family: 'Lato'; -fx-font-size: 10;");

        Text font2 = new Text("Indie Flower");
        font2.setStyle("-fx-font-family: 'Indie Flower'; -fx-font-size: 10;");

        Text font3 = new Text("Oxygen");
        font3.setStyle("-fx-font-family: 'Oxygen'; -fx-font-size: 10;");

        Text font4 = new Text("Pacifico");
        font4.setStyle("-fx-font-family: 'Pacifico'; -fx-font-size: 10;");

        Text font5 = new Text("Old Standard TT");
        font5.setStyle("-fx-font-family: 'Old Standard TT'; -fx-font-size: 10;");

        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(font1.getText(), font2.getText(), font3.getText(), font4.getText(), font5.getText()));
        cb.setValue(font1.getText());
        cb.setTooltip(new Tooltip("Select the font"));
        textEdit.getChildren().add(cb);

        Label label = new Label("ENTER THE TEXT");
        TextArea text = new TextArea();
        text.setText(pageModel.getSelectedComponent().getText_box());
        Button okay = new Button("OK");
        Label message = new Label("Action Event to update the Heading.");
        pane.getChildren().addAll(textEdit, label, text, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Lato");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Indie+Flower");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Oxygen");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Pacifico");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Old+Standard+TT");
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        addHyperlink.setOnAction(f -> {
            if (text.getSelectedText() != null) {
                Stage linkStage = new Stage();
                VBox linkPane = new VBox();
                Label command = new Label("Enter Link:");
                TextField title = new TextField();
                Button done = new Button("OK");
                Label linkMessage = new Label("Link for \"" + text.getSelectedText() + "\".");
                linkPane.getChildren().addAll(command, title, done, linkMessage);
                Scene linkScene = new Scene(linkPane);
                linkScene.getStylesheets().add(STYLE_SHEET_UI);
                linkPane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
                linkStage.setScene(linkScene);
                linkStage.show();
                done.setOnAction(g -> {
                    Hyperlink link = new Hyperlink(text.getSelectedText(), title.getText());
                    pageModel.getSelectedComponent().getText_links().add(link);
                    linkStage.close();
                    System.out.println(pageModel.getSelectedComponent().getText_links().size());
                });
            }
        });
        editHyperlink.setOnAction(f -> {
            if (text.getSelectedText() != null) {
                Stage linkStage = new Stage();
                VBox linkPane = new VBox();
                Label command = new Label("Enter Link:");
                TextField title = new TextField();
                Button done = new Button("OK");
                Label linkMessage = new Label("Link for \"" + text.getSelectedText() + "\".");
                linkPane.getChildren().addAll(command, title, done, linkMessage);
                Scene linkScene = new Scene(linkPane);
                linkScene.getStylesheets().add(STYLE_SHEET_UI);
                linkPane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
                linkStage.setScene(linkScene);
                linkStage.show();
                done.setOnAction(g -> {
                    Hyperlink link = new Hyperlink(text.getSelectedText(), title.getText());
                    pageModel.getSelectedComponent().getText_links().add(link);
                    linkStage.close();
                    System.out.println(pageModel.getSelectedComponent().getText_links().size());
                });
            }
        });
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setText_box(text.getText());
            reloadComponentPane();
        });
    }

    public void inputDialogBox(String messageText, String inputCommand) {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        Label label = new Label(inputCommand);
        TextField title = new TextField();
        Button okay = new Button("OK");
        Label message = new Label(messageText);
        pane.getChildren().addAll(label, title, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
        });
    }

    public void inputList(boolean newList) {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        ScrollPane paneScrollView = new ScrollPane(pane);
        Label title = new Label("ADD ITEMS HERE.");
        VBox items = new VBox();

        HBox item1 = new HBox();
        TextField itemList = new TextField();

        item1.getChildren().addAll(itemList);

        item1.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        pane.getChildren().add(title);
        Button addItem = this.initChildButton(pane, ICON_ADD_SLIDE, TOOLTIP_ADD_LIST_ITEM, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        Button ok = new Button("Ok");
        listComponent = FXCollections.observableArrayList();

        if (listComponent.size() > 0) {
            for (String listItem : pageModel.getSelectedComponent().getLists()) {
                System.out.println(listItem);
                HBox newItem = new HBox();
                newItem.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
                TextField nextItemList = new TextField();
                nextItemList.setText(listItem);
                newItem.getChildren().addAll(nextItemList);
                Button nextRemove = this.initChildButton(newItem, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_LIST_ITEM, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
                items.getChildren().add(newItem);
                pane.getChildren().removeAll(items, addItem, ok);
                pane.getChildren().addAll(items, addItem, ok);
                nextRemove.setOnAction(e1 -> {
                    items.getChildren().remove(newItem);
                    listComponent.remove(listItem);
                });
            }
        }

        //items.getChildren().add(item1);
        addItem.setOnAction(e2 -> {
            HBox newItem = new HBox();
            newItem.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            TextField nextItemList = new TextField();
            newItem.getChildren().addAll(nextItemList);
            Button nextRemove = this.initChildButton(newItem, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_LIST_ITEM, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
            items.getChildren().add(newItem);
            pane.getChildren().removeAll(items, addItem, ok);
            pane.getChildren().addAll(items, addItem, ok);
            listItem = nextItemList.getPromptText();
            listComponent.add(listItem);
            nextRemove.setOnAction(e1 -> {
                items.getChildren().remove(newItem);
                listComponent.remove(listItem);
            });
        });

        Scene scene = new Scene(paneScrollView);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.setMinWidth(450);
        newStage.setMinHeight(600);
        newStage.show();
        ok.setOnAction(e1 -> {
            newStage.close();
            if (newList) {
                //newComponent = new Component("List", listComponent);
                pageModel.addListComponent(listComponent);
                reloadComponentPane();
            }
        });
    }

    public void videoInput() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        HBox videoSelectionWindow = new HBox();
        VBox captionVBox;
        Label captionLabel, imgHeight, imgWidth;
        TextField captionTextField, imgHeightField, imgWidthField;
        VideoSelectionController videoController;

        mediaSelectionView = new MediaView();
        updateVideo(mediaSelectionView);

        Button okay = new Button("OK");

        captionVBox = new VBox();
        //Component image = new Component("Image", DEFAULT_SLIDE_IMAGE, props.getProperty(DEFAULT_IMAGE_CAPTION));
        captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
        captionTextField = new TextField();
        if (pageModel.getSelectedComponent().getCaption() != null) {
            captionTextField.setText(pageModel.getSelectedComponent().getCaption());
        }
        imgHeight = new Label("Video Height");
        imgHeightField = new TextField();
        imgHeightField.setText(Double.toString(mediaSelectionView.getFitWidth()));
        imgWidth = new Label("Video Width");
        imgWidthField = new TextField();
        imgWidthField.setText(Double.toString(mediaSelectionView.getFitHeight()));
        captionVBox.getChildren().add(captionLabel);
        captionVBox.getChildren().add(captionTextField);
        captionVBox.getChildren().addAll(imgHeight, imgHeightField, imgWidth, imgWidthField);
        captionVBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        videoSelectionWindow.getChildren().add(mediaSelectionView);
        videoSelectionWindow.getChildren().add(captionVBox);

        // SETUP THE EVENT HANDLERS
        videoController = new VideoSelectionController();
        mediaSelectionView.setOnMousePressed(e -> {
            videoController.processSelectVideo(this, pageModel);
        });
        pane.getChildren().addAll(videoSelectionWindow, okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            pageModel.getSelectedComponent().setCaption(captionTextField.getText());
        });
    }

    public void updateVideo(MediaView videoSelectionView) {
        String videoPath = pageModel.getSelectedComponent().getMedia_link();
        File file = new File(videoPath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Media video = new Media(videoPath);
            MediaPlayer mediaPlayer = new MediaPlayer(video);
            mediaSelectionView.setMediaPlayer(mediaPlayer);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / video.getWidth();
            double scaledHeight = video.getHeight() * perc;
            mediaSelectionView.setFitWidth(scaledWidth);
            mediaSelectionView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            ErrorHandler eH = new ErrorHandler(this);
            eH.processError(props.getProperty(ERROR), props.getProperty(ALERT_VIDEO_NOT_FOUND));
        }
    }
}
