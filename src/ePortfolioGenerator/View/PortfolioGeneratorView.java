/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator.View;

import ePortfolioGenerator.Controller.FileController;
import ePortfolioGenerator.Controller.ImageSelectionController;
import ePortfolioGenerator.Controller.PortfolioFileManager;
import ePortfolioGenerator.Error.ErrorHandler;
import ePortfolioGenerator.LanguagePropertyType;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_COMPONENT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_FOOTER;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_IMAGE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_PAGE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_SLIDE_SHOW;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_TEXT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_ADD_VIDEO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EDIT_BANNER;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EDIT_STUDENT_NAME;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EDIT_STYLE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EXIT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_EXPORT_PORTFOLIO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_LOAD_PORTFOLIO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_NEW_PORTFOLIO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_REMOVE_COMPONENT;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_REMOVE_PAGE;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_SAVE_AS_PORTFOLIO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_SAVE_PORTFOLIO;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_TOGGLE_TO_EDITOR;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_TOGGLE_TO_VIEWER;
import static ePortfolioGenerator.LanguagePropertyType.TOOLTIP_UPDATE_PAGE_TITLE;
import ePortfolioGenerator.Model.Page;
import ePortfolioGenerator.Model.PortfolioModel;
import ePortfolioGenerator.StartupConstants;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_DIALOG_BOX_STYLE;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_FILE_TOOLBAR_HBOX;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_PAGE_TOOLBAR_HBOX;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_TEXT_TITLE_SELECT;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_TEXT_TITLE_STYLE;
import static ePortfolioGenerator.StartupConstants.DEFAULT_BANNER_IMAGE;
import static ePortfolioGenerator.StartupConstants.DEFAULT_BANNER_WIDTH;
import static ePortfolioGenerator.StartupConstants.DEFAULT_FOOTER;
import static ePortfolioGenerator.StartupConstants.DEFAULT_STUDENT_NAME;
import static ePortfolioGenerator.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_COMPONENT;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_FOOTER;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_IMAGE;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_PAGE;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_TEXT;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_VIDEO;
import static ePortfolioGenerator.StartupConstants.ICON_CHANGE_STUDENT_NAME;
import static ePortfolioGenerator.StartupConstants.ICON_EDIT_BANNER;
import static ePortfolioGenerator.StartupConstants.ICON_EDIT_STYLE;
import static ePortfolioGenerator.StartupConstants.ICON_EXIT;
import static ePortfolioGenerator.StartupConstants.ICON_EXPORT_PORTFOLIO;
import static ePortfolioGenerator.StartupConstants.ICON_LOAD_PORTFOLIO;
import static ePortfolioGenerator.StartupConstants.ICON_NEW_PORTFOLIO;
import static ePortfolioGenerator.StartupConstants.ICON_REMOVE_COMPONENT;
import static ePortfolioGenerator.StartupConstants.ICON_REMOVE_PAGE;
import static ePortfolioGenerator.StartupConstants.ICON_SAVE_AS_PORTFOLIO;
import static ePortfolioGenerator.StartupConstants.ICON_SAVE_PORTFOLIO;
import static ePortfolioGenerator.StartupConstants.ICON_TOGGLE_TO_EDITOR;
import static ePortfolioGenerator.StartupConstants.ICON_TOGGLE_TO_VIEWER;
import static ePortfolioGenerator.StartupConstants.ICON_UPDATE_PAGE_TITLE;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import static ePortfolioGenerator.StartupConstants.PATH_IMAGES;
import static ePortfolioGenerator.StartupConstants.STYLE_SHEET_UI;
import java.io.File;
import java.net.URL;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Rahul
 */
public class PortfolioGeneratorView {

    Stage primaryStage;
    Scene primaryScene;

    BorderPane mainPane;
    HBox fileToolbar;
    HBox pageToolbar;
    HBox workspace;

    VBox sideBar;
    VBox siteViewer;
    VBox headerEditor;

    ScrollPane pagesNavScollPane;
    VBox pagesNavPane;

    Button newPortfolio;
    Button loadPortfolio;
    Button savePortfolio;
    Button saveAsPortfolio;
    Button exportPortfolio;
    Button changeStudentName;
    Button exit;

    Button addPage;
    Button removePage;
    Button changeStyle;
    Button addFooter;

    Button toggleWorkspace;

    Button editBanner;
    Button editPageTitle;

    PortfolioModel pages;

    Label page1_title, page2_title;

    PageEditView pageView;

    String studentName;
    String bannerImage;
    String footer;
    String pageTitle;
    String styleSheet;

    boolean editFooter;

    ImageView imageSelectionView;

    FileController fileController;

    PortfolioFileManager fileManager;

    ErrorHandler errorHandler;

    public PortfolioGeneratorView(PortfolioFileManager initFileManager) {
        fileManager = initFileManager;
        pages = new PortfolioModel(this);
        errorHandler = new ErrorHandler(this);
    }

    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    public void initFileToolbar() {
        fileToolbar = new HBox();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fileToolbar.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_HBOX);
        newPortfolio = this.initChildButton(fileToolbar, ICON_NEW_PORTFOLIO, TOOLTIP_NEW_PORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadPortfolio = this.initChildButton(fileToolbar, ICON_LOAD_PORTFOLIO, TOOLTIP_LOAD_PORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        savePortfolio = this.initChildButton(fileToolbar, ICON_SAVE_PORTFOLIO, TOOLTIP_SAVE_PORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveAsPortfolio = this.initChildButton(fileToolbar, ICON_SAVE_AS_PORTFOLIO, TOOLTIP_SAVE_AS_PORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportPortfolio = this.initChildButton(fileToolbar, ICON_EXPORT_PORTFOLIO, TOOLTIP_EXPORT_PORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        changeStudentName = this.initChildButton(fileToolbar, ICON_CHANGE_STUDENT_NAME, TOOLTIP_EDIT_STUDENT_NAME, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exit = this.initChildButton(fileToolbar, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    public void initWorkspace() {
        workspace = new HBox();

        pageToolbar = new HBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        pageToolbar.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_HBOX);
        addPage = this.initChildButton(pageToolbar, ICON_ADD_PAGE, TOOLTIP_ADD_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        removePage = this.initChildButton(pageToolbar, ICON_REMOVE_PAGE, TOOLTIP_REMOVE_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        changeStyle = this.initChildButton(pageToolbar, ICON_EDIT_STYLE, TOOLTIP_EDIT_STYLE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        pagesNavPane = new VBox();
        page1_title = new Label("Trial Page 1");
        page1_title.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        page2_title = new Label("Trial Page 2");
        page2_title.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
        //pagesNavPane.getChildren().addAll(page1_title, page2_title);
        sideBar = new VBox();

        editBanner = this.initChildButton(pageToolbar, ICON_EDIT_BANNER, TOOLTIP_EDIT_BANNER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        addFooter = this.initChildButton(pageToolbar, ICON_ADD_FOOTER, TOOLTIP_ADD_FOOTER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);

        editPageTitle = this.initChildButton(pageToolbar, ICON_UPDATE_PAGE_TITLE, TOOLTIP_UPDATE_PAGE_TITLE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);

        siteViewer = new VBox();
        toggleWorkspace = this.initChildButton(pageToolbar, ICON_TOGGLE_TO_VIEWER, TOOLTIP_TOGGLE_TO_VIEWER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);

        sideBar.getChildren().add(pageToolbar);

        // NOW PUT THESE TWO IN THE WORKSPACE
        //workspace.getChildren().add(pageToolbar);
        //workspace.getChildren().add(sideBar);
        //workspace.getChildren().add(portfolioEditorPane);
        //workspace.getChildren().add(portfolioEditorScollPane);
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        pagesNavScollPane = new ScrollPane(pagesNavPane);
        sideBar.getChildren().add(pagesNavScollPane);

        mainPane = new BorderPane();
        mainPane.setTop(fileToolbar);
        //mainPane.setLeft(sideBar);
        primaryScene = new Scene(mainPane);

        //this.updateToolbarControls(false);
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public void startUI(Stage initPrimaryStage, String windowTitle) {

        editFooter = false;
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }

    public void updateToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        mainPane.setCenter(workspace);
        mainPane.setLeft(sideBar);

        // NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
        savePortfolio.setDisable(saved);
        saveAsPortfolio.setDisable(saved);
        exportPortfolio.setDisable(false);
        changeStudentName.setDisable(false);
        editBanner.setDisable(false);
        addFooter.setDisable(false);

        updatePageEditToolbarControls(true);
    }

    public void updatePageEditToolbarControls(boolean edit) {
        // AND THE SLIDESHOW EDIT TOOLBAR
        addPage.setDisable(false);
        removePage.setDisable(edit);
        changeStyle.setDisable(edit);
        editPageTitle.setDisable(edit);
        toggleWorkspace.setDisable(edit);
    }

    public void reloadPageNavPane() {
        pagesNavPane.getChildren().clear();
        workspace.getChildren().clear();
        for (Page page : pages.getPages()) {
            Label pageOnNav = new Label(page.getPageTitle());
            pageOnNav.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
            pageOnNav.setMinHeight(50);
            pageOnNav.setMinWidth(525);
            styleSheet = page.getPageStyle();
            if (pages.getOpenPage() == null) {
                workspace.getChildren().clear();
                updatePageEditToolbarControls(true);
            } else {
                if (pages.isOpenPage(page)) {
                    pageOnNav.getStyleClass().add(CSS_CLASS_TEXT_TITLE_SELECT);
                    if (page.getPageStyle() == "Style2.css" || page.getPageStyle() == "Style3.css" || page.isHasBanner()) {
                        pageView = new PageEditView(page, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                        editBanner.setDisable(false);
                    } else {
                        pageView = new PageEditView(page, pages.getStudentName(), pages.getFooter());
                        editBanner.setDisable(true);
                    }
                    updatePageEditToolbarControls(false);
                    workspace.getChildren().clear();
                    workspace.getChildren().add(pageView);
                } else {
                    pageOnNav.getStyleClass().add(CSS_CLASS_TEXT_TITLE_STYLE);
                }
            }
            pagesNavPane.getChildren().add(pageOnNav);
            pageOnNav.setOnMousePressed(e -> {
                if (page.isHasBanner()) {
                    pageView = new PageEditView(page, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                    updatePageEditToolbarControls(false);
                } else {
                    pageView = new PageEditView(page, pages.getStudentName(), pages.getFooter());
                    updatePageEditToolbarControls(false);
                }
                workspace.getChildren().clear();
                workspace.getChildren().add(pageView);
                pages.setOpenPage(page);
                pageOnNav.getStyleClass().add(CSS_CLASS_TEXT_TITLE_SELECT);
                this.reloadPageNavPane();
            });
        }
    }

    public ImageView getImageSelectionView() {
        return imageSelectionView;
    }

    public PortfolioModel getPortfolioModel() {
        return pages;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public void initEventHandlers() {
        fileController = new FileController(this, fileManager);

        newPortfolio.setOnAction(e -> {
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Label label = new Label("Enter the name of the student");
            TextField title = new TextField();
            Button okay = new Button("OK");
            Label message = new Label("This is the action event for the new Portfolio.");
            pane.getChildren().addAll(label, title, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            okay.setOnAction(f -> {
                pages.reset();
                newStage.close();
                studentName = title.getText();
                pages.setStudentName(studentName);
                StartupConstants.DEFAULT_STUDENT_NAME = studentName;
                updateToolbarControls(false);
            });
        });
        loadPortfolio.setOnAction(e -> {
            fileController.handleLoadPortfolioRequest(pageView);
            savePortfolio.setDisable(true);
            updateToolbarControls(false);
            reloadPageNavPane();
        });
        savePortfolio.setOnAction(e -> {
            savePortfolio.setDisable(true);
            boolean save = fileController.handleSavePortfolioRequest(pages);
        });
        saveAsPortfolio.setOnAction(e -> {
            saveAsPortfolio.setDisable(true);
            boolean save = fileController.handleSaveAsPortfolioRequest(pages, this);
        });
        exportPortfolio.setOnAction(e -> {
            File HTMLFile = pages.generateWebPages(0);

            WebView webview = new WebView();
            webview.getEngine().load(HTMLFile.toURI().toString());

            Stage viewSlideShow = new Stage();
            viewSlideShow.setTitle("e-Portfolio");

            viewSlideShow.setScene(new Scene(webview, 1500, 1000));
            viewSlideShow.show();
            /*
             File HTMLFile = new File("sites/TrialWebpage/index.html");
             WebView webview = new WebView();
             webview.getEngine().load(HTMLFile.toURI().toString());

             Stage viewPage = new Stage();
             VBox pane = new VBox();
             pane.getChildren().add(webview);
             viewPage.setTitle("TRIAL PAGE");

             viewPage.setScene(new Scene(pane, 1500, 1000));
             viewPage.show();*/
        });
        changeStudentName.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Label label = new Label("Enter the name of the student");
            TextField title = new TextField();
            title.setText(pages.getStudentName());
            Button okay = new Button("OK");
            Label message = new Label("This is the action event for the changing the name of the student.");
            pane.getChildren().addAll(label, title, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            okay.setOnAction(f -> {
                newStage.close();
                studentName = title.getText();
                pages.setStudentName(studentName);
                if (!editFooter) {
                    pages.setFooter(DEFAULT_FOOTER.replace(DEFAULT_STUDENT_NAME, studentName));
                }
                reloadPageNavPane();
            });
        });
        exit.setOnAction(e -> {
            primaryStage.close();
        });
        addPage.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            int sheetvalue;
            String[] CSS_Files = {"Style1", "Style2", "Style3", "Style4", "Style5"};
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Label label = new Label("Enter the title of the page");
            TextField title = new TextField();
            Button okay = new Button("OK");
            Label styleCB = new Label("Choose ONE Page Style");
            ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList("Style 1: No Banner, Center Horizontal Nav Bar",
                    "Style 2: Banner, Center Horizontal Nav Bar",
                    "Style 3: Banner, Left Vertical Nav Bar",
                    "Style 4: No Banner, Left Vertical Nav Bar",
                    "Style 5: No Banner, Left Horizontal Nav Bar"));
            Label message = new Label("This is the action event for the new page.");
            pane.getChildren().addAll(label, title, styleCB, cb, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue ov, Number value, Number new_value) {
                    styleSheet = CSS_Files[new_value.intValue()];
                }
            });
            okay.setOnAction(f -> {
                pageTitle = title.getText();
                newStage.close();
                if (styleSheet == "Style2") {
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                    pages.addPage(pageTitle, true, styleSheet, pageView);
                    editBanner.setDisable(false);
                } else if (styleSheet == "Style3") {
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                    pages.addPage(pageTitle, true, styleSheet, pageView);
                    editBanner.setDisable(false);
                } else {
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getFooter());
                    pages.addPage(pageTitle, styleSheet, pageView);
                    editBanner.setDisable(true);
                }
                workspace.getChildren().clear();
                workspace.getChildren().add(pageView);
                updatePageEditToolbarControls(true);
            });
        });
        removePage.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            pages.removeSelectedPage();
            updatePageEditToolbarControls(true);
        });
        addFooter.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            editFooter = true;
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Label label = new Label("Enter the footer of the page");
            TextField title = new TextField();
            title.setText(pages.getFooter());
            Button okay = new Button("OK");
            Label message = new Label("This is the action event to edit he footer.");
            pane.getChildren().addAll(label, title, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            okay.setOnAction(f -> {
                newStage.close();
                footer = title.getText();
                pages.setFooter(footer);
                reloadPageNavPane();
            });
        });
        toggleWorkspace.setOnAction(e -> {
            File HTMLFile = pages.generateWebPages(pages.getPages().indexOf(pages.getOpenPage()));
            WebView webview = new WebView();
            webview.getEngine().load(HTMLFile.toURI().toString());
            webview.setMinWidth(1375);
            VBox pane = new VBox();
            Button moveToEditor;
            moveToEditor = this.initChildButton(pane, ICON_TOGGLE_TO_EDITOR, TOOLTIP_TOGGLE_TO_EDITOR, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
            pane.getChildren().add(webview);
            workspace.getChildren().clear();
            workspace.getChildren().add(pane);

            moveToEditor.setOnAction(f -> {
                workspace.getChildren().clear();
                reloadPageNavPane();
            });
        });
        editPageTitle.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Label label = new Label("Enter the title of the page");
            TextField title = new TextField();
            title.setText(pages.getOpenPage().getPageTitle());
            Button okay = new Button("OK");
            Label message = new Label("This is the action event for updating the title of the page.");
            pane.getChildren().addAll(label, title, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            okay.setOnAction(f -> {
                newStage.close();
                pages.getOpenPage().setPageTitle(title.getText());
                reloadPageNavPane();
            });
        });
        changeStyle.setOnAction(e -> {
            savePortfolio.setDisable(false);
            saveAsPortfolio.setDisable(false);
            String[] CSS_Files = {"Style1", "Style2", "Style3", "Style4", "Style5"};
            Stage newStage = new Stage();
            VBox pane = new VBox();
            Button okay = new Button("OK");
            Label styleCB = new Label("Choose ONE Page Style");
            ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList("Style 1: No Banner, Center Horizontal Nav Bar",
                    "Style 2: Banner, Center Horizontal Nav Bar",
                    "Style 3: Banner, Left Vertical Nav Bar",
                    "Style 4: No Banner, Left Vertical Nav Bar",
                    "Style 5: No Banner, Left Horizontal Nav Bar"));
            Label message = new Label("This is the action event for the new page.");
            pane.getChildren().addAll(styleCB, cb, okay, message);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
            newStage.setScene(scene);
            newStage.show();
            cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue ov, Number value, Number new_value) {
                    styleSheet = CSS_Files[new_value.intValue()];
                }
            });
            okay.setOnAction(f -> {
                newStage.close();
                if (styleSheet == "Style2.css") {
                    pages.getOpenPage().setPageStyle(styleSheet);
                    pages.getOpenPage().setHasBanner(true);
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                    editBanner.setDisable(false);
                } else if (styleSheet == "Style3.css") {
                    pages.getOpenPage().setPageStyle(styleSheet);
                    pages.getOpenPage().setHasBanner(true);
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getBannerImageLink(), pages.getFooter());
                    editBanner.setDisable(false);
                } else {
                    pages.getOpenPage().setPageStyle(styleSheet);
                    pages.getOpenPage().setHasBanner(false);
                    pageView = new PageEditView(pageTitle, pages.getStudentName(), pages.getFooter());
                    editBanner.setDisable(true);
                }
                workspace.getChildren().clear();
                workspace.getChildren().add(pageView);
                updatePageEditToolbarControls(true);
                reloadPageNavPane();
                pageView.reloadComponentPane();
            });
            reloadPageNavPane();
            pageView.reloadComponentPane();
        });
        editBanner.setOnAction(e -> {
            ImageInput();
        });
    }

    public void dialogBox(String message) {
        Stage newStage = new Stage();
        BorderPane pane = new BorderPane();
        Label label = new Label(message);
        pane.setCenter(label);
        Button okay = new Button("OK");
        pane.setBottom(okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
        });
    }

    public String inputDialogBox(String messageText, String inputCommand) {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        Label label = new Label(inputCommand);
        TextField title = new TextField();
        Button okay = new Button("OK");
        Label message = new Label(messageText);
        pane.getChildren().addAll(label, title, okay, message);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
        });
        return title.getText();
    }

    public void ImageInput() {
        Stage newStage = new Stage();
        VBox pane = new VBox();
        ImageSelectionController imageController;

        Label title = new Label("Select your banner image.");

        imageSelectionView = new ImageView();
        updateImage();

        Button okay = new Button("OK");
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        // SETUP THE EVENT HANDLERS
        imageController = new ImageSelectionController();
        imageSelectionView.setOnMousePressed(e -> {
            imageController.processSelectImage(this, pages);
        });
        pane.getChildren().addAll(title, imageSelectionView, okay);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add(CSS_CLASS_DIALOG_BOX_STYLE);
        newStage.setScene(scene);
        newStage.show();
        okay.setOnAction(f -> {
            newStage.close();
            reloadPageNavPane();
        });
    }

    /**
     * This function gets the image for the slide and uses it to update the
     * image displayed.
     */
    public void updateImage() {
        String imagePath = pages.getBannerImageLink();
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_BANNER_WIDTH;
            double perc = scaledWidth / slideImage.getWidth();
            double scaledHeight = slideImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            ErrorHandler eH = new ErrorHandler(this);
            //eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
        }
    }
}
