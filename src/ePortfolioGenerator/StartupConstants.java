package ePortfolioGenerator;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author Rahul S Agasthya
 */
public class StartupConstants {
    public static String ENGLISH_LANG = "English";

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./Data/LanguageData/";
    public static String PATH_PORTFOLIO_SAVE = "./Data/Portfolios/";
    public static String PATH_IMAGES = "./Images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_VIDEOS = "./Videos/";
    public static String PATH_CSS = "./ePortfolioGenerator/Style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "ePortfolioGeneratorStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "ApplicationIcon.png";
    public static String ICON_NEW_PORTFOLIO = "NewPortfolio.png";
    public static String ICON_LOAD_PORTFOLIO = "Load.png";
    public static String ICON_SAVE_PORTFOLIO = "Save.png";
    public static String ICON_SAVE_AS_PORTFOLIO = "SaveAs.png";
    public static String ICON_EXPORT_PORTFOLIO = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "NewPage.png";
    public static String ICON_REMOVE_PAGE = "RemovePage.png";
    public static String ICON_EDIT_STYLE = "EditStyle.png";
    
    public static String ICON_NEW_SLIDE_SHOW = "New.png";
    public static String ICON_LOAD_SLIDE_SHOW = "Load.png";
    public static String ICON_SAVE_SLIDE_SHOW = "Save_1.png";
    public static String ICON_VIEW_SLIDE_SHOW = "View.png";
    public static String ICON_ADD_SLIDE = "Add.png";
    public static String ICON_REMOVE_SLIDE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    
    public static String ICON_ADD_COMPONENT = "AddComponent.png";
    public static String ICON_REMOVE_COMPONENT = "RemoveComponent.png";
    public static String ICON_TOGGLE_TO_EDITOR = "ToggleToEditor.png";
    public static String ICON_TOGGLE_TO_VIEWER = "ToggleToViewer.png";
    public static String ICON_CHANGE_STUDENT_NAME = "RenameTitle.png";
    
    public static String ICON_ADD_HEADING = "AddHeading.png";
    public static String ICON_ADD_IMAGE = "AddImage.png";
    public static String ICON_ADD_VIDEO = "AddVideo.png";
    public static String ICON_ADD_SLIDE_SHOW = "AddSlideShow.png";
    public static String ICON_ADD_TEXT = "AddText.png";
    public static String ICON_ADD_LIST = "AddList.png";
    
    public static String ICON_EDIT_BANNER = "EditBanner.png";
    public static String ICON_UPDATE_PAGE_TITLE = "UpdateTitle.png";
    
    
    public static String ICON_ADD_HYPERLINK = "AddLink.png";
    public static String ICON_EDIT_HYPERLINK = "EditLink.png";
    public static String ICON_ADD_FOOTER = "AddFooter.png";

    // UI SETTINGS
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static String    DEFAULT_BANNER_IMAGE = PATH_IMAGES + "CSBanner.jpg";
    public static int	    DEFAULT_BANNER_WIDTH = 500;
    public static int	    DEFAULT_BANNER_HEIGHT = 250;
    public static String    DEFAULT_STUDENT_NAME = "\"Student Name\"";
    public static String    DEFAULT_FOOTER = "This work belongs to " + DEFAULT_STUDENT_NAME;
    public static String    DEFAULT_VIDEO = "SampleVideo.mp4";
    
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_SLIDE_SHOW_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW = "page_edit_view";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_slide_edit_view";
    public static String    CSS_CLASS_FILE_TOOLBAR_HBOX = "portfolio_toolbar_HBox";
    public static String    CSS_CLASS_PAGE_TOOLBAR_HBOX = "page_toolbar_HBox";
    public static String    CSS_CLASS_TEXT_TITLE_STYLE = "title_view";
    public static String    CSS_CLASS_TEXT_TITLE_SELECT = "selected_title_view";
    public static String    CSS_CLASS_DIALOG_BOX_STYLE = "dialog_box";
    
    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
    public static String    OK_BUTTON_TEXT = "OK";
}
